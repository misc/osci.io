---
title: Infrastructure Administration
---

## Introduction

[OSCI maintained services](/infra_and_services/) are maintained using the [OSAS/community-cage-infra-ansible repository](https://gitlab.com/osas/community-cage-infra-ansible) on GitLab. Contributions are welcome.

## Deployment

### Configuration Management

This repository contains Ansible playbooks to deploy VMs and configure hosts to provide the service. Only a few secrets (tokens, or emails to avoid SPAM) are kept private using Ansible Vault.

Some communities (RDO, oVirt…) possess their own resources with an internal Infra team; in this case the deployment rules are hosted in their own repositories.

### Packaging

Along with these rules we might need specific packages for certain softwares. For example we use the Mailman 3 repository maintained by a fellow developer in the `mailman3` Ansible role. We also maintain [our own repository](/infra_rpm_repository/) for important fixes.

### Specific Procedures

* Blade Management:
    * [Blade Hardware Configuration](/infra_procedures/blade_hw_config/)
    * [Blade OS Installation](/infra_procedures/blade_os_install/)
    * [Blade OS Setup](/infra_procedures/blade_os_setup/)

## Resources

Our resources are located in the Community Cage in the RDU datacenter. OSCI is managed as a separate tenant of the Community Cage with the following resources:

* [Network VLANs and IPs](/infra_res_net/)
* [Hardware](/infra_res_hw/)

## Administration Access

### Direct Access

All machines with a public IP address are currently reachable through SSH with keys. OSAS admins keys are automatically installed via Ansible, user root access may also be available on a case-by-case basis.

### Internal Access

All machines in VLANs without publics IPs can be reached through a jump host. Each tenant's administrator should have their SSH key(s) registered first.

You can reach the host using its internal DNS name or IP by adding the `-o ProxyJump=tenant@soeru.osci.io` option to SSH.

To simplify administration of your hosts via Ansible, you can create a `osci_internal_zone` group with all hosts in internal VLANs, and create `group_vars/osci_internal_zone/connection.yml` containing:

```yaml
---
ansible_ssh_common_args: '-o ProxyJump=tenant@soeru.osci.io'
```


### Management Access

All bare metal machines or equipments are accessible for administration tasks via the management VLAN:

* SSH gives access to a shell or a CLI (for switches or CMM)
* CMM or blade admin interfaces both allow web UI and IPMI access

### Fallback Access

In case we totally break network/SSH configuration on bare metal hosts, access via a console server is not possible. Each host is accessible by connecting via SSH on `conserve.adm.osci.io` on a specific port:

| SSH Port | Host                |
|:--------:| :-------------------|
| 7001     | Speedy              |
| 7002     | Guido               |
| 7003     | Catatonic Switch A1 |
| 7004     | Catatonic Switch A2 |
| 7005     | Catatonic CMM 2     |
| 7006     | Catatonic CMM 1     |
| 7007     | TempusFugit         |

You first need to authenticate with your console server account, and then you can access a direct console on the host.

Using Ctrl-z (even via SSH) allows you to access a menu and quit.

OSAS admins can login as root via SSH to the standard port to access a UNIX shell. The `portaccessmenu` command allows you to list available machines and connect to them. Be aware that you need to authenticate with your console server account first even if you're already logged in. The `configmenu` command is used to setup the device (users, groups, ports, ACLs…). The device configuration has been saved (manually) on file.rdu.redhat.com:/mnt/share/OSAS/backups/Conserve/ so please update it if needed.

