---
title: Network
---

| VLANs             | ID  | IP Ranges                           | Gateway       | Purpose                                                |
|:-----------------:|:---:|:-----------------------------------:|:-------------:|:-------------------------------------------------------|
| OSAS-Public       | 190 | 8.43.85.193-222<br/>8.43.85.225-238 | 8.43.85.254   | direct Internet access                                 |
| OSAS-Provisioning | 430 | 172.24.30.1-249                     | 172.24.30.254 | provisioning of new hosts                              |
| OSAS-Management   | 431 | 172.24.31.1-249                     | 172.24.31.254 | equipments administration                              |
| OSAS-Internal     | 432 | 172.24.32.1-249                     | 172.24.32.254 | machines behind a reverse proxy (security, saves IPs…) |

